# R pkgs repository
echo 'options(repos=c(CRAN="https://cloud.r-project.org"))' >> ~/.Rprofile

# compilation flags for R pkgs
mkdir -p ~/.R
echo 'CFLAGS=-O3 -mtune=native' >> ~/.R/Makevars
echo 'CXXFLAGS=-O3 -mtune=native' >> ~/.R/Makevars

## use C locale for R
echo 'LC_ALL=C' >> ~/.Renviron

